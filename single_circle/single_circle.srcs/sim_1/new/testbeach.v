`timescale 1ns / 1ps

module testbeach( );

	reg clk;
	reg rst;
	
	computer computer1(clk,rst);

	initial begin
		$readmemh("D:/codes/gits/single-cycle-cpu/inst/inst1.txt.mem",computer1.inst_rom_1.mem);
		rst = 1'b0;
		clk = 1'b0;
		#50 rst = 1'b1;
	end

	always
		#10 clk = ~clk;

endmodule
