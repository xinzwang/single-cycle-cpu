`timescale 1ns / 1ps

`include "macro.vh"

module my_cpu(
	input clk,
	input rst,
	output [7:0]pc,
	// inst rom
	output inst_rom_en,
	output [31:0]inst_rom_addr,
	input  [31:0]inst_rom_data,
	// data ram
	output data_ram_en,
	output [ 3:0]data_ram_wen,
	output [31:0]data_ram_addr,
	input  [31:0]data_ram_rdata,
	output [31:0]data_ram_wdata
	
    );

	// pc lines
	wire [1:0]pc_oper;
	wire [31:0]pc_br;
	wire [31:0]pc_jmp;
	wire [31:0]pc_out;
	// regs lines
	wire reg_wen;
	wire [4:0] reg_raddr1;
	wire [4:0] reg_raddr2;
	wire [4:0] reg_waddr;
	wire [31:0] reg_wdata;
	wire [31:0] reg_rdata1;
	wire [31:0] reg_rdata2;
	// alu lines
	wire [3:0] alu_op;
	wire [31:0]alu_num1;
	wire [31:0] alu_num2;
	wire [31:0] alu_res;
	wire alu_err;
	
	assign pc = pc_out[9:2];

	assign inst_rom_en = rst;
	assign data_ram_en = rst;
	assign inst_rom_addr = pc_out;	//ָ���ַ

	// pc
	pc pc_1(
		.clk(clk),
		.rst(rst),
		.pc_oper(pc_oper),
		.pc_br(pc_br),
		.pc_jmp(pc_jmp),
		.pc_out(pc_out)
	);
	
	// �Ĵ�����ջ
	regs regs_1(
		.clk(clk),
		.rst(rst),
		.wen(reg_wen),
		.raddr1(reg_raddr1),
		.raddr2(reg_raddr2),
		.waddr(reg_waddr),
		.wdata(reg_wdata),
		.rdata1(reg_rdata1),
		.rdata2(reg_rdata2)
	);
	
	// alu
	alu alu_1(
		.clk(clk),
		.rst(rst),
		.oper(alu_op),
		.num1(alu_num1),
		.num2(alu_num2),
		.res(alu_res),
		.err(alu_err)
	);

	// control unit
	my_cu cu_1(
		.clk(clk),
		.rst(rst),
		.inst(inst_rom_data),

		.pc_oper(pc_oper),
		.pc_jmp(pc_jmp),
		.pc_br(pc_br),

		.alu_op(alu_op),
		.alu_num1(alu_num1),
		.alu_num2(alu_num2),
		.alu_res(alu_res),		
			
		.ram_wen(data_ram_wen),		//ramдʹ��
		.ram_addr(data_ram_addr),
		.ram_wdata(data_ram_wdata),
		.ram_rdata(data_ram_rdata),

		.reg_wen(reg_wen),			//�Ĵ���дʹ��
		.reg_waddr(reg_waddr),
		.reg_wdata(reg_wdata),
		.reg_raddr1(reg_raddr1),
		.reg_raddr2(reg_raddr2),
		.reg_rdata1(reg_rdata1),
		.reg_rdata2(reg_rdata2)	
	);
endmodule
