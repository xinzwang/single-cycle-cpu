`timescale 1ns / 1ps

module data_ram(
	input clk,
	input en,
	input [3:0]wen,
	input [31:0]addr,
	input [31:0]wdata,
	output [31:0]rdata
    );

	// 256 Byte
	reg [7:0]mem[255:0];

	wire [7:0]_addr = addr[7:0];
	reg _en;
	reg [3:0]_wen;
	always @(*) begin
		_en <= en;
		_wen <= wen;
	end

    integer i;
	always  @(posedge clk) begin
		if(!_en) begin
			for(i = 0; i <= 255; i  = i + 1) begin
				mem[i] <= 8'b00000000;
			end
		end else begin
			if(_wen) begin
				mem[_addr+3] <= wdata[31:25];
				mem[_addr+2] <= wdata[24:16];
				mem[_addr+1] <= wdata[15:8];
				mem[_addr] <= wdata[7:0];
			end
		end
	end
		
	assign rdata = _en ? {mem[_addr+3],mem[_addr+2],mem[_addr+1],mem[_addr]} : 32'b0;

endmodule
