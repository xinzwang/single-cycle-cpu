`define PC_NORMAL	2'b00
`define PC_JMP		2'b01
`define PC_BR		2'b10
`define PC_STOP		2'b11

`define ALU_ADD     4'h0
`define ALU_SUB     4'h1
`define ALU_AND     4'h2
`define ALU_OR      4'h3
`define ALU_SLT     4'h4
`define ALU_SLL		4'h5
`define ALU_NULL    4'h6

`define INST_OP_LW		6'b100011
`define INST_OP_SW		6'b101011
`define INST_OP_BEQ		6'b000100
`define INST_OP_LUI		6'b001111
`define INST_OP_ADDIU	6'b001001

`define INST_OP_J		6'b000010

`define INST_OP_FUNC	6'b000000

`define INST_FUNC_ADD	6'b100000
`define INST_FUNC_SUB	6'b100010
`define INST_FUNC_SLL	6'b000000
