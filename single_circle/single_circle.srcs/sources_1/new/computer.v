`timescale 1ns / 1ps

`include "macro.vh"

module computer(
    input sys_clk,
    input sys_rst,
    
    output [7:0]light
    );
    
    wire rst = sys_rst;
    
    integer cnt = 0;
    reg clk;
    always @(posedge sys_clk) begin
       if (sys_rst == 0) begin
            cnt <= 0;
            clk <= 0;
       end else begin
//            if(cnt >20000000 )begin
            if(cnt > 4)begin
                clk = ~clk;
                cnt <= 0;
            end else begin
                cnt = cnt + 1;
            end
       end
    end
    
    
    
    
    
	// inst rom bus
    wire        inst_rom_en;
	wire [31:0] inst_rom_addr;
	wire [31:0] inst_rom_data;
	// data ram bus
	wire        data_ram_en;
	wire [ 3:0] data_ram_wen;
	wire [31:0] data_ram_addr;
	wire [31:0] data_ram_wdata;
	wire [31:0] data_ram_rdata;

	// cpu
	my_cpu cpu_1(
		.clk(clk),
		.rst(rst),
		.pc(light),
		.inst_rom_en(inst_rom_en),
		.inst_rom_addr(inst_rom_addr),
		.inst_rom_data(inst_rom_data),
		.data_ram_en(data_ram_en),
		.data_ram_wen(data_ram_wen),
		.data_ram_addr(data_ram_addr),
		.data_ram_rdata(data_ram_rdata),
		.data_ram_wdata(data_ram_wdata)
	);
	
	// instruction ROM 1
	inst_rom inst_rom_1(
		.clk(clk),
		.en(inst_rom_en),
		.addr(inst_rom_addr),
		.data(inst_rom_data)
	);

	// data ram 1
	data_ram data_ram_1(
		.clk(clk),
		.en(data_ram_en),
		.wen(data_ram_wen),
		.addr(data_ram_addr),
		.wdata(data_ram_wdata),
		.rdata(data_ram_rdata)
	);

	


    
endmodule
