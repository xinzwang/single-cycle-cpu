`timescale 1ns / 1ps

module inst_rom(
	input clk,
	input en,
	input [31:0]addr,
	output [31:0]data
    );

	// 256 Byte
	reg [7:0]mem[255:0];

	reg _en;
	always @(posedge clk)
		_en <= en;	

	wire [7:0]_addr = addr[7:0];

	assign data = en ? addr[31]==1'b0 ? {mem[_addr+3],mem[_addr+2],mem[_addr+1],mem[_addr]} : 32'b0 : 32'b0;
	
endmodule
