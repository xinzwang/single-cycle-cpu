`timescale 1ns / 1ps

`include "macro.vh"

module regs(
	input clk,
	input rst,

	input wen,
	input [4:0] raddr1,
	input [4:0] raddr2,
	input [4:0] waddr,
	input [31:0] wdata,

	output [31:0] rdata1,
	output [31:0] rdata2
    );

	// register stack
	reg [31:0]regs[31:0];

	assign rdata1 = (raddr1 == 5'b0 ? 32'b0 : regs[raddr1]);
	assign rdata2 = (raddr2 == 5'b0 ?32'b0 : regs[raddr2]);

	// on write reg
	integer i;
	always @(posedge clk or negedge rst) begin
		if(!rst) begin
			for(i = 0; i <= 31; i  = i + 1) begin
				regs[i] <= 32'b0;
			end
		end else if (wen == 1'b1 && waddr != 5'b00000) begin
			regs[waddr] <= wdata;
		end
	end

endmodule
