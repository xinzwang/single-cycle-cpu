`timescale 1ns / 1ps

`include "macro.vh"

module pc(
	input clk,
	input rst,
	input [1:0]pc_oper,
	input [31:0]pc_br,
	input [31:0]pc_jmp,
	output [31:0]pc_out
    );

	reg[31:0] pc;		//pc�Ĵ���
	wire [31:0]pc_ds;	// delay slot

	assign pc_out = pc;
	assign pc_ds = pc +32'h4;	//delay slot
    
	always @(posedge clk or negedge rst) begin
		if(rst == 1'b0) begin
			pc <= 32'hfffffff0;
//		end if (pc[31] == 1'b1) begin
//			pc <= pc;
		end else begin
			case (pc_oper)
				`PC_STOP: begin
					pc <= pc;
				end
				`PC_NORMAL: begin
					pc <= pc + 4;
				end
				`PC_JMP: begin
					pc <= {pc_ds[31:28],pc_jmp[25:0],2'b00};
				end 
				`PC_BR: begin
					pc <= pc_ds + (pc_br << 2'h2);
				end
			endcase
		end
	end




endmodule
